import { danger, warn } from 'danger';
import * as fs from 'fs';

// Filters
const filesOnly = (file: string) =>
  fs.existsSync(file) && fs.lstatSync(file).isFile();

// changed files include new files
const changedFiles = danger.git.modified_files.concat(danger.git.created_files);

// Any source files changed?
const sourceChanges = changedFiles
  .filter(path => path.includes('src/'))
  .filter(paths => filesOnly);

// A proper MR description is expected
if (danger.gitlab.mr.description.length < 10) {
  warn('Please include a description of your MR changes.');
}

// When source files have changed, expect Changelog to be updated
if (sourceChanges.length > 0 && !changedFiles.includes('CHANGELOG.md')) {
  warn('Please include a Changelog entry for your MR.');
}
