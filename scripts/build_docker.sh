#!/bin/sh

# Automagically build all docker image for services
# 
# Process is described below:
#  1. Build base image for current monorepo state
#  2. Loop over directories in ./services
#  2.1. Assert "package.json" and "Dockerfile" exists (otherwise skip)
#  2.2. Extract name from package.json
#  2.3. Create docker_tag => Remove '@' symbol from package_name
#  2.4. Build docker image

docker build -t sensorbucket/builder:dev -f docker/Dockerfile.builder .

for serviceDirectory in services/*/ ; do
  # Make sure that directory is a package/module with Docker support
  if [[ ! -d "$serviceDirectory" && -f "$serviceDirectory/package.json" && -f "$serviceDirectory/Dockerfile" ]] ; then continue; fi
  # Extract package name
  package_name=$(cat $serviceDirectory/package.json | perl -n -e'/\"name\":[ ]\"(.*)\"\,/ && print $1')
  # Remove illegal characters
  # package_name=$(echo $package_name | perl -n -e'/s/\@/[]/r/')
  docker_tag=${package_name//[@]}:dev
  #
  printf "Found package $package_name...\n"
  printf "Building docker image $docker_tag...\n"
  # Build docker image
  docker build -t $docker_tag -f "$serviceDirectory/Dockerfile" --build-arg UID=$(id -u) --build-arg GID=$(id -g) .
done