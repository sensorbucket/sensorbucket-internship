# Multiflexmeter Device Worker

This worker receives data encoded in the device-type specific format. This worker is responsible for translating these messages in generic messages which the system understands.