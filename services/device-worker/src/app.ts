import { Config } from './config';
import { AMQPConnection } from '@sensorbucket/mq-amqp';
import { logger } from '@sensorbucket/logging';
import { DeviceDataInterface } from '@sensorbucket/common';
import { MQMessage } from '@sensorbucket/message-queue';

interface WorkerScript {
  setup(logger: any): Promise<void>;
  work(dto: DeviceDataInterface): Promise<any>;
}

class WorkerBootstrap {
  private connection!: AMQPConnection;
  private worker!: WorkerScript;

  /**
   * Connect to the AMQP Server
   */
  private async connectToQueue(): Promise<void> {
    try {
      this.connection = new AMQPConnection(Config.get().AMQP_URI);
      await this.connection.connect();
    } catch (e) {
      logger.error(
        { eventCode: 'worker.device.mfm.connectToQueue.error', error: e },
        'Could not connect to AMQP Server! Exiting...',
      );
      process.exit(1);
    }
  }

  /**
   * Check if config is valid
   */
  private validateConfig(): void {
    // Ensure QUEUE_NAME and WORKER are set
    const queueName = Config.get().QUEUE_NAME;
    const workerName = Config.get().WORKER;
    if (
      queueName === 'QUEUE_NOT_SPECIFIED' ||
      workerName === 'WORKER_NOT_SPECIFIED'
    ) {
      logger.error({}, 'Missing QUEUE_NAME or WORKER in config file');
      process.exit(1);
    }
  }

  /**
   * Load ../config.json if exists
   */
  private async loadConfig(): Promise<void> {
    try {
      await Config.loadConfig('../config.json');
      this.validateConfig();
    } catch (e) {
      // Let the user know the configuration is reverting to defaults
      if (e.code === 'MODULE_NOT_FOUND') {
        logger.warn(
          { eventCode: 'worker.device.loadConfig.error' },
          'Could not load ../config.json, using defaults',
        );
      }
    }
  }

  /**
   * Load and setup the worker script
   */
  private async loadWorker() {
    try {
      const workerName = Config.get().WORKER;
      // Load worker script
      this.worker = require(`../scripts/${workerName}`);
      // Call setup
      await this.worker.setup(logger);
      // Throw error if no script was found
    } catch (e) {
      if (e.code === 'MODULE_NOT_FOUND') {
        logger.error(
          { eventCode: 'worker.device.loadWorker.error' },
          'Worker package not found',
        );
        process.exit(1);
      } else {
        logger.error(e);
      }
    }
  }

  /**
   *
   * @param message Received message from the MQ
   */
  private async onMessage(message: MQMessage): Promise<any> {
    const dto = message.body;
    // Try to process message
    try {
      const result = await this.worker.work(dto);
      logger.info(
        { eventCode: 'worker.device.result ', result },
        'Finished Job',
      );
      // All good, acknowledge message
      message.acknowledge();
    } catch (e) {
      // If payload was invalid, (n)ack message to avoid loop
      // TODO: nack Message!
      if (e.code === 'INVALID_PAYLOAD') {
        message.acknowledge();
      }
      logger.error(
        { eventCode: 'worker.device.script.error', error: e },
        'Device worker script error',
      );
    }
  }

  /**
   * Start listening to the queue
   */
  private async startListening(): Promise<void> {
    const sourceQueue = await this.connection.openChannel();
    sourceQueue.addListener(Config.get().QUEUE_NAME, this.onMessage.bind(this));
  }

  /**
   * Start the worker
   */
  async start(): Promise<void> {
    await this.loadConfig();
    await this.connectToQueue();
    await this.loadWorker();
    await this.startListening();
  }
}

new WorkerBootstrap().start().then(() => {
  logger.info({ eventCode: 'worker.device.started' }, 'Worker started');
});
