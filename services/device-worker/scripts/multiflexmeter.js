/**
 * Validate the received buffer
 */
const validatePayload = buffer => {
  const checks = [buffer.length >= 6];
  if (checks.includes(false)) {
    throw { code: 'INVALID_PAYLOAD' };
  }
};

module.exports = {
  // Called once
  setup: async logger => {
    this.logger = logger;
    logger.info(
      { eventCode: 'worker.device.script.multiflexmeter.setup.success' },
      'Multiflexmeter script succesfully loaded',
    );
  },
  // Do conversion
  work: async dto => {
    let buffer = Buffer.from(dto.deviceData, 'base64');
    validatePayload(buffer);
    return {
      distance_to_water: buffer.readUInt16LE(0),
      air_temperature: buffer.readFloatLE(2),
    };
  },
};
