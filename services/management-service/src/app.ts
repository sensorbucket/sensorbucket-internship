import { logger } from '@sensorbucket/logging';
import Koa from 'koa';
import Router from 'koa-router';
import { Controller } from '@sensorbucket/common';
import { Client as PSQLClient } from 'pg';
import bodyParser from 'koa-bodyparser';
import {
  DeviceTypeRepository,
  DeviceTypeService,
  DeviceTypeController,
} from './deviceTypes';
import {
  SourceTypeRepository,
  SourceTypeService,
  SourceTypeController,
} from './sourceTypes';
import { DeviceRepository, DeviceService, DeviceController } from './devices';
import {
  LocationController,
  LocationService,
  LocationRepository,
} from './locations';
import { SourceRepository, SourceService, SourceController } from './sources';
import { Config } from './config';

/**
 * The management service owns the Device, Source, Locations models
 */
class ManagementService {
  private koaServer: Koa;
  private koaRouter: Router;

  private database!: PSQLClient;

  private sourceTypeRepo!: SourceTypeRepository;
  private deviceTypeRepo!: DeviceTypeRepository;
  private deviceRepo!: DeviceRepository;
  private sourceRepo!: SourceRepository;
  private locationRepo!: LocationRepository;

  private sourceTypeService!: SourceTypeService;
  private deviceTypeService!: DeviceTypeService;
  private deviceService!: DeviceService;
  private sourceService!: SourceService;
  private locationService!: LocationService;

  constructor() {
    this.koaServer = new Koa();
    this.koaServer.use(bodyParser());
    this.koaRouter = new Router();
  }

  /**
   * Register a controller
   */
  registerController(path: string, controller: Controller): void {
    const router = new Router();
    controller.setupRoutes(router);
    this.koaRouter.use(path, router.routes());
  }

  async loadDatabase(): Promise<void> {
    try {
      // Connect to database
      // TODO: Use config
      this.database = new PSQLClient({
        connectionString: Config.get().POSTGRES_URI,
      });
      await this.database.connect();
      // Test connection
      await this.database.query('SELECT 1 + 1 AS result');
      logger.info(
        { eventCode: 'service.management.database.connected' },
        'Connected to database',
      );
      // Load repositories
      this.sourceTypeRepo = new SourceTypeRepository(this.database);
      this.deviceTypeRepo = new DeviceTypeRepository(this.database);
      this.locationRepo = new LocationRepository(this.database);
      this.deviceRepo = new DeviceRepository(this.database);
      this.sourceRepo = new SourceRepository(this.database);
    } catch (e) {
      logger.error({ error: e }, 'Database error occured!');
      process.exit(1);
    }
  }

  /**
   * Load internal services
   */
  loadServices(): void {
    this.sourceTypeService = new SourceTypeService(this.sourceTypeRepo);
    this.deviceTypeService = new DeviceTypeService(this.deviceTypeRepo);
    this.locationService = new LocationService(this.locationRepo);
    this.sourceService = new SourceService(
      this.sourceRepo,
      this.sourceTypeService,
    );
    this.deviceService = new DeviceService(
      this.deviceRepo,
      this.deviceTypeService,
      this.sourceService,
      this.locationService,
    );
  }

  /**
   * Load all controllers and register them with the router
   */
  loadControllers(): void {
    // Register SourceTypeController
    this.registerController(
      '/sourceTypes',
      new SourceTypeController(this.sourceTypeService),
    );
    // Register DeviceController
    this.registerController(
      '/sources',
      new SourceController(this.sourceService),
    );
    // Register DeviceController
    this.registerController(
      '/devices',
      new DeviceController(this.deviceService),
    );
    // Register DeviceTypeController
    this.registerController(
      '/devicetypes',
      new DeviceTypeController(this.deviceTypeService),
    );
    // Register DeviceController
    this.registerController(
      '/locations',
      new LocationController(this.locationService),
    );
    // Register health check
    this.koaRouter.get('/', (ctx: Koa.Context) => {
      ctx.status = 200;
    });
    // Use registered routes
    this.koaServer.use(this.koaRouter.routes());
  }

  /**
   * Start the service
   */
  async start(): Promise<void> {
    await Config.loadConfig('../config.js');
    await this.loadDatabase();
    this.loadServices();
    this.loadControllers();
    this.koaServer.listen(3000);
  }
}

// Instantiate and start service
const service = new ManagementService();
service.start().then(() => {
  logger.info(
    { eventCode: 'service.management.started' },
    'Management Service started',
  );
});
