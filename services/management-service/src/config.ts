/**
 * This is the format the configuration has to adhere to
 */
interface ManagementServiceConfigInterface {
  POSTGRES_URI: string;
}

export class Config {
  // Default configuration
  private static configMap: ManagementServiceConfigInterface = {
    POSTGRES_URI:
      'postgres://sensorbucket-management:databasepassword@database:5432/management',
  };

  /**
   * Get the configuration object
   */
  static get(): ManagementServiceConfigInterface {
    return this.configMap;
  }

  /**
   * Load a configuration file
   *
   * Any options not specified will be set to their defaults.
   *
   * @param path Path to the configuration file
   */
  static async loadConfig(filePath: string): Promise<void> {
    const config: ManagementServiceConfigInterface = require(filePath);
    // TODO: could allow for arbitrary data to be inserted. (Keys that the interface does not specify)
    // Override defaults
    this.configMap = {
      ...this.configMap,
      ...config,
    };
  }
}
