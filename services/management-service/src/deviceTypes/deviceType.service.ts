import { DeviceTypeRepository } from './deviceType.repository';
import { DeviceType } from '@sensorbucket/models';
import { HasPrimaryKey } from '@sensorbucket/resources/src';

export class DeviceTypeService implements HasPrimaryKey<DeviceType> {
  private repo: DeviceTypeRepository;

  private propertiesRequired = ['name', 'version'];

  constructor(repo: DeviceTypeRepository) {
    this.repo = repo;
  }

  /**
   * Ensure object has all required Device attributes
   * @param raw The raw object
   */
  private validateDeviceTypeObject(raw: Object): boolean {
    return this.propertiesRequired.every(attr => attr in raw);
  }

  /**
   * Return all existing Device Types
   */
  getDeviceTypes(): Promise<DeviceType[]> {
    return this.repo.listAll();
  }

  /**
   * Get a Device Type by its id
   * @param id The id of the Device Type
   */
  getDeviceType(id: number): Promise<DeviceType | null> {
    return this.repo.findById(id);
  }

  /**
   * Get a Device Type by its id
   * @param id The id of the Device Type
   */
  get(id: number): Promise<DeviceType | null> {
    return this.getDeviceType(id);
  }

  /**
   *
   * @param deviceType An object representing a device type
   */
  createDeviceType(raw: Object): Promise<DeviceType> {
    // Are required properties present?
    const valid = this.validateDeviceTypeObject(raw);
    if (!valid) {
      throw new Error('Bad request');
    }
    const device = DeviceType.from(raw);
    return this.repo.insert(device);
  }

  /**
   * Delete a DeviceType by its ID
   * @param id The id of the DeviceType
   */
  deleteDeviceType(id: number): Promise<boolean> {
    return this.repo.deleteById(id);
  }
}
