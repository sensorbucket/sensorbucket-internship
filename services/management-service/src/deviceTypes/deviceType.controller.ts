import { Controller } from '@sensorbucket/common';
import Router from 'koa-router';
import { Context, ParameterizedContext } from 'koa';
import { DeviceTypeService } from './deviceType.service';

export class DeviceTypeController extends Controller {
  private service: DeviceTypeService;

  constructor(devicetypeService: DeviceTypeService) {
    super();
    this.service = devicetypeService;
  }

  setupRoutes(router: Router): void {
    router.get('/', this.getAllDeviceTypes.bind(this));
    router.get('/:id', this.getDeviceType.bind(this));
    router.post('/', this.createDeviceType.bind(this));
    router.delete('/:id', this.deleteDeviceType.bind(this));
  }

  /**
   * Return all devicetypes this user owns
   * @apiMethod GET
   * @apiPath /devicetypes/
   */
  private async getAllDeviceTypes(ctx: Context): Promise<void> {
    // TODO: infer user ID
    try {
      ctx.body = await this.service.getDeviceTypes();
      ctx.status = 200;
    } catch (e) {
      ctx.status = 500;
      console.error(e);
    }
  }

  private async getDeviceType(ctx: Context): Promise<void> {
    // TODO: infer user ID
    try {
      const devicetype = await this.service.getDeviceType(ctx.params.id);
      if (devicetype === null) {
        ctx.status = 404;
        return;
      }
      ctx.body = devicetype;
      ctx.status = 200;
    } catch (e) {
      ctx.status = 500;
      console.error(e);
    }
  }

  /**
   * Create a new devicetype
   * @apiMethod POST
   * @apiPath /devicetypes/
   */
  private async createDeviceType(ctx: ParameterizedContext): Promise<void> {
    try {
      const devicetype = await this.service.createDeviceType(ctx.request.body);
      ctx.body = devicetype;
      ctx.status = 200;
      return;
    } catch (e) {
      ctx.status = 500;
      console.error(e);
    }
  }

  /**
   * Delete a devicetype by its id
   * @apiMethod DELETE
   * @apiPath /devicetypes/:id/
   */
  private async deleteDeviceType(ctx: ParameterizedContext): Promise<void> {
    try {
      const devicetypeId = parseInt(ctx.params.id);
      // Check if user is authorized
      const deleted = await this.service.deleteDeviceType(devicetypeId);
      ctx.status = deleted ? 200 : 404;
    } catch (e) {
      ctx.status = 500;
      console.error(e);
    }
  }
}
