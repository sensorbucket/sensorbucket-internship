import { DeviceType } from '@sensorbucket/models';
import { Client as PSQLClient, QueryResultRow } from 'pg';

export class DeviceTypeRepository {
  static readonly tableName = 'device_types';

  private database: PSQLClient;

  constructor(database: PSQLClient) {
    this.database = database;
  }

  /**
   * Map a row to a Device Type model
   * @param row A query row representing a Device Type
   */
  private deviceTypeFromRow(row: QueryResultRow): DeviceType {
    return new DeviceType(
      row.id,
      row.name,
      row.version,
      row.configuration_schema,
    );
  }

  /**
   * List all device types
   */
  public async listAll(): Promise<DeviceType[]> {
    const execution = this.database.query(
      `SELECT * FROM ${DeviceTypeRepository.tableName}`,
    );
    const result = await execution;
    return result.rows.map(this.deviceTypeFromRow);
  }

  /**
   * Find a DeviceType by its id
   * @param id The id of the device type
   */
  public async findById(id: number): Promise<DeviceType | null> {
    const query = {
      text: `SELECT * FROM ${DeviceTypeRepository.tableName} WHERE id = $1`,
      values: [id],
    };
    const execution = this.database.query(query);
    const result = await execution;
    // Return null if not found
    if (result.rowCount < 1) {
      return null;
    }
    return this.deviceTypeFromRow(result.rows[0]);
  }

  /**
   * Insert a new device into the database
   * @param deviceType DeviceType object to insert
   */
  public async insert(deviceType: DeviceType): Promise<DeviceType> {
    const query = {
      text: `INSERT INTO ${DeviceTypeRepository.tableName} (name, version, configuration_schema) VALUES ($1, $2, $3) RETURNING *`,
      values: [
        deviceType.name,
        deviceType.version,
        deviceType.configurationSchema,
      ],
    };
    const execution = this.database.query(query);
    const result = await execution;
    // Throw if insert failed
    if (result.rowCount < 1) {
      throw new Error('Failed to insert new DeviceType');
    }
    return this.deviceTypeFromRow(result.rows[0]);
  }

  /**
   * Remove a DeviceType from the database
   * @param id DeviceType to remove
   */
  public async deleteById(id: number): Promise<boolean> {
    const query = {
      text: `DELETE FROM ${DeviceTypeRepository.tableName} WHERE id = $1`,
      values: [id],
    };
    const execution = this.database.query(query);
    const result = await execution;
    return result.rowCount > 0;
  }
}
