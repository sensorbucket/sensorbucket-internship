import { Device } from '@sensorbucket/models';
import { logger } from '@sensorbucket/logging';
import { Client as PSQLClient, QueryResultRow } from 'pg';

export class DeviceRepository {
  static readonly tableName = 'devices';

  private database: PSQLClient;

  constructor(database: PSQLClient) {
    this.database = database;
  }

  /**
   * Map a row to a Device model
   * @param row A query row representing a device
   */
  private deviceFromRow(row: QueryResultRow): Device {
    return new Device(
      row.id,
      row.name,
      row.owner,
      row.device_type,
      row.source,
      row.location,
      row.device_configuration,
      row.source_configuration,
    );
  }

  /**
   * List all existing devices
   */
  public async listAll(): Promise<Device[]> {
    const execution = this.database.query(
      `SELECT * FROM ${DeviceRepository.tableName}`,
    );
    const result = await execution;
    return result.rows.map(this.deviceFromRow);
  }

  /**
   * List all devices where owner equals given ID
   * @param id The id of the owner
   */
  public async listWhereowner(id: number): Promise<Device[]> {
    const query = {
      text: `SELECT * FROM ${DeviceRepository.tableName} WHERE owner = $1`,
      values: [id],
    };
    const execution = this.database.query(query);
    const result = await execution;
    return result.rows.map(this.deviceFromRow);
  }

  /**
   * Return a device by its di
   * @param id Device Id
   */
  public async findById(id: number): Promise<Device | null> {
    const query = {
      text: `SELECT * FROM ${DeviceRepository.tableName} WHERE id = $1 LIMIT 1`,
      values: [id],
    };
    const execution = this.database.query(query);
    const result = await execution;
    // Return Null query returned nothing
    if (result.rowCount < 1) {
      return null;
    }

    return this.deviceFromRow(result.rows[0]);
  }

  /**
   * Inserts a new device into the database
   * @param device Device object
   */
  public async insert(device: Device): Promise<Device> {
    const query = {
      text: `INSERT INTO ${DeviceRepository.tableName} (name, owner, location, device_type, source, device_configuration, source_configuration) VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING *`,
      values: [
        device.name,
        device.owner,
        device.location,
        device.deviceType,
        device.source,
        device.deviceConfiguration,
        device.sourceConfiguration,
      ],
    };
    const execution = this.database.query(query);
    const result = await execution;
    // Throw if insert failed
    if (result.rowCount < 1) {
      throw new Error('Failed to insert new device');
    }
    return this.deviceFromRow(result.rows[0]);
  }

  /**
   * Remove a device from the database
   * @param id Device to destroy
   */
  public async deleteById(id: number): Promise<boolean> {
    const query = {
      text: `DELETE FROM ${DeviceRepository.tableName} WHERE id = $1`,
      values: [id],
    };
    const execution = this.database.query(query);
    const result = await execution;
    return result.rowCount > 0;
  }
}
