import { Controller } from '@sensorbucket/common';
import Router from 'koa-router';
import { Context, ParameterizedContext, Next, Middleware } from 'koa';
import { DeviceService } from './device.service';
import { Device } from '@sensorbucket/models';
import { ExpandMiddlewareFactory, ExpandMeta } from '@sensorbucket/resources';
import { logger } from '@sensorbucket/logging';
import { keys, every, pick } from 'lodash';

export class DeviceController extends Controller {
  private deviceService: DeviceService;
  private expandResponse: Middleware;

  private filterWhitelist = [
    { field: 'sourceConfiguration', nesting: true },
    { field: 'deviceConfiguration', nesting: true },
  ];

  constructor(deviceService: DeviceService) {
    super();
    this.deviceService = deviceService;
    this.expandResponse = ExpandMiddlewareFactory<Device>(
      this.deviceService.expander,
    ).bind(this);
  }

  setupRoutes(router: Router): void {
    // Setup expand middleware
    router.get('/', this.getAllDevices.bind(this), this.expandResponse);
    router.get('/:id', this.getDevice.bind(this), this.expandResponse);
    router.post('/', this.createDevice.bind(this), this.expandResponse);
    router.delete('/:id', this.deleteDevice.bind(this));
  }

  private getByPath(obj: any, path: string) {
    for (
      var i = 0, pathParts = path.split('.'), len = pathParts.length;
      i < len;
      i++
    ) {
      if (obj === undefined || obj === null) return obj;
      obj = obj[pathParts[i]];
    }
    return obj;
  }

  private getFilter(ctx: Context): { [index: string]: any }[] {
    const allowedPaths = this.filterWhitelist.map(filter => filter.field);
    // Create matchable object
    const queries = keys(ctx.query).map(path => ({
      field: path.split('.')[0],
      path,
      value: ctx.query[path],
    }));

    return queries.filter(query => allowedPaths.includes(query.field));
  }

  /**
   * TODO: If nested path in relational field then expand the field and continue
   * TODO: Refactor for re-usability and proper variable typing
   * @param device
   * @param filters
   */
  private applyFilter(
    device: Device,
    filters: { [index: string]: any }[],
  ): boolean {
    return every(
      filters.map(filter => {
        const value = this.getByPath(device, filter.path);
        return value === filter.value;
      }),
    );
  }

  /**
   * Return all devices this user owns
   * @apiMethod GET
   * @apiPath /devices/
   */
  private async getAllDevices(ctx: Context, next: Next): Promise<void> {
    // TODO: infer user ID
    try {
      const filters = this.getFilter(ctx);
      const devices = (
        await this.deviceService.getDevicesForUser(0)
      ).filter(device => this.applyFilter(device, filters));
      ctx.body = devices;
      ctx.status = 200;
      return next();
    } catch (e) {
      ctx.status = 500;
      console.error(e);
    }
  }

  private async getDevice(ctx: Context, next: Next): Promise<void> {
    // TODO: infer user ID
    try {
      const device = await this.deviceService.getDevice(ctx.params.id);
      if (device === null) {
        ctx.status = 404;
        return;
      }
      ctx.body = device;
      ctx.status = 200;
      return next();
    } catch (e) {
      ctx.status = 500;
      console.error(e);
    }
  }

  /**
   * Create a new device
   * @apiMethod POST
   * @apiPath /devices/
   */
  private async createDevice(
    ctx: ParameterizedContext,
    next: Next,
  ): Promise<void> {
    try {
      const device = await this.deviceService.createDeviceForUser(
        0,
        ctx.request.body,
      );
      ctx.body = device;
      ctx.status = 200;
      return next();
    } catch (e) {
      // Return proper error
      ctx.status = e.httpCode || 500;
      // If this is a user error, inform them correctly
      if (e.httpCode && e.httpCode >= 400 && e.httpCode < 500) {
        ctx.body = e;
      }
      console.error(e);
    }
  }

  /**
   * Delete a device by its id
   * @apiMethod DELETE
   * @apiPath /devices/:id/
   */
  private async deleteDevice(
    ctx: ParameterizedContext,
    next: Next,
  ): Promise<void> {
    try {
      const deviceId = parseInt(ctx.params.id);
      // Check if user is authorized
      const deleted = await this.deviceService.deleteDevice(deviceId);
      ctx.status = deleted ? 200 : 404;
      return next();
    } catch (e) {
      ctx.status = 500;
      console.error(e);
    }
  }
}
