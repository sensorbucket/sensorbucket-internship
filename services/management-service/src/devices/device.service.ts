import { DeviceRepository } from './device.repository';
import { Device } from '@sensorbucket/models';
import { pick } from 'lodash';
import { DeviceTypeService } from '../deviceTypes';
import { SourceService } from '../sources';
import { logger } from '@sensorbucket/logging';
import { DeviceRelationExpander } from './device.expander';
import { LocationService } from '../locations';

type ExpandFunction = (device: Device, nestedPath?: string) => Promise<any>;

export class DeviceService {
  private repo: DeviceRepository;
  public readonly expander: DeviceRelationExpander;

  private propertiesRequired = ['name', 'deviceType'];
  private propertiesWritable = [
    'name',
    'location',
    'deviceType',
    'source',
    'deviceConfiguration',
    'sourceConfiguration',
  ];

  constructor(
    repo: DeviceRepository,
    deviceTypeService: DeviceTypeService,
    sourceService: SourceService,
    locationService: LocationService,
  ) {
    this.repo = repo;
    this.expander = new DeviceRelationExpander(
      deviceTypeService,
      sourceService,
      locationService,
    );
  }

  /**
   * Ensure an object is a valid Device resource.
   * This includes having the required properties
   * and having valid existing relations.
   * @param raw The raw object
   */
  private async validateDeviceObject(raw: Object): Promise<string[]> {
    const errors: string[] = [];
    // Are all properties present?
    const missingProperties = this.propertiesRequired.filter(
      attr => !(attr in raw),
    );
    if (missingProperties.length > 0) {
      errors.push(
        'Missing required attributes: ' + missingProperties.join(','),
      );
    }
    // Are all not-null relations valid?
    const invalidRelations = await this.expander.validateRelations(raw);
    if (invalidRelations.length > 0) {
      errors.push('Invalid relation on: ' + invalidRelations.join(','));
    }
    return errors;
  }

  /**
   * Return all devices a user can read
   * @param user The user
   */
  getDevicesForUser(user: any): Promise<Device[]> {
    return this.repo.listWhereowner(user);
  }

  /**
   * Return a device by its ID
   * @param id The id of the device
   */
  getDevice(id: number): Promise<Device | null> {
    return this.repo.findById(id);
  }

  /**
   * Creates a new device for a specific user
   * @param userId The User ID of the owner
   * @param deviceRaw An object representing a Device
   */
  async createDeviceForUser(
    userId: number,
    deviceRaw: Object,
  ): Promise<Device> {
    // Is the given object valid? (fields present, relations exist)
    const errors = await this.validateDeviceObject(deviceRaw);
    if (errors.length > 0) {
      throw { httpCode: 400, message: 'Device is not valid', details: errors };
    }
    const device = Device.from(deviceRaw);
    device.owner = userId;
    return this.repo.insert(device);
  }

  /**
   * Delete a device by its ID
   * @param id The id of the device
   */
  deleteDevice(id: number): Promise<boolean> {
    return this.repo.deleteById(id);
  }
}
