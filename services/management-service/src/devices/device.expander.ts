import { RelationExpander, FieldRelation } from '@sensorbucket/resources';
import { SourceService } from '../sources';
import { DeviceTypeService } from '../deviceTypes';
import { LocationService } from '../locations';

export class DeviceRelationExpander extends RelationExpander {
  constructor(
    private readonly deviceTypeService: DeviceTypeService,
    private readonly sourceService: SourceService,
    private readonly locationService: LocationService,
  ) {
    super();
  }

  relations = [
    new FieldRelation('deviceType', this.deviceTypeService),
    new FieldRelation(
      'source',
      this.sourceService,
      this.sourceService.expander,
    ),
    new FieldRelation('location', this.locationService),
  ];
}
