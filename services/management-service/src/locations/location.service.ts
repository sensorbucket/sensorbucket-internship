import { LocationRepository } from './location.repository';
import { Location } from '@sensorbucket/models';
import { pick as filterAttributes } from 'lodash';
import { HasPrimaryKey } from '@sensorbucket/resources/src';

export class LocationService implements HasPrimaryKey<Location> {
  private repo: LocationRepository;

  private propertiesRequired = ['name', 'latitude', 'longitude'];
  private propertiesWritable = ['name', 'latitude', 'longitude'];

  constructor(repo: LocationRepository) {
    this.repo = repo;
  }

  /**
   * Ensure an object is a valid Location resource.
   * This includes having the required properties
   * and having valid existing relations.
   * @param raw The raw object
   */
  private async validateLocationObject(raw: Object): Promise<string[]> {
    const errors: string[] = [];
    // Are all properties present?
    const missingProperties = this.propertiesRequired.filter(
      attr => !(attr in raw),
    );
    if (missingProperties.length > 0) {
      errors.push(
        'Missing required attributes: ' + missingProperties.join(','),
      );
    }

    /**
     * TODO: Location does not have existing relations yet
     * (owner resource does not exist yet)
     */
    // Are all not-null relations valid?
    // const invalidRelations = await this.expander.validateRelations(raw);
    // if (invalidRelations.length > 0) {
    //   errors.push('Invalid relation on: ' + invalidRelations.join(','));
    // }
    return errors;
  }

  /**
   * Return all locations a user can read
   * @param user The user
   */
  getLocationsForUser(user: any): Promise<Location[]> {
    return this.repo.listWhereowner(user);
  }

  /**
   * Return a location by its ID
   * @param id The ID of the location
   */
  getLocation(id: any): Promise<Location | null> {
    return this.repo.findById(id);
  }

  /**
   * Return a location by its ID
   * @param id The ID of the location
   */
  get(id: any): Promise<Location | null> {
    return this.getLocation(id);
  }

  /**
   * Create a new location for a user
   * @param userId The User ID of the owner
   * @param locationRaw An object representing a Location
   */
  async createLocationForUser(
    userId: number,
    locationRaw: object,
  ): Promise<Location> {
    // All required properties present?
    const errors = await this.validateLocationObject(locationRaw);
    if (errors.length > 0) {
      throw { httpCode: 400, message: 'Device is not valid', details: errors };
    }
    // Create model
    const filtered = filterAttributes(locationRaw, this.propertiesWritable);
    const location = Location.from(filtered);
    location.owner = userId;
    return this.repo.insert(location);
  }

  /**
   * Delete a location
   */
  deleteLocation(id: number): Promise<boolean> {
    return this.repo.deleteById(id);
  }
}
