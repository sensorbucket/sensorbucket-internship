import { Location } from '@sensorbucket/models';
import { logger } from '@sensorbucket/logging';
import { Client as PSQLClient, QueryResultRow } from 'pg';

export class LocationRepository {
  static readonly tableName = 'locations';

  private database: PSQLClient;

  constructor(database: PSQLClient) {
    this.database = database;
  }

  /**
   * Map a row to a Location model
   * @param row A query row representing a location
   */
  private locationFromRow(row: QueryResultRow): Location {
    const geometry = JSON.parse(row.geom);
    // Create base location
    const location = new Location(row.id, row.name, row.owner, 0, 0);
    // Modify coordinates based on GIS geometry
    location.geometry = geometry;
    return location;
  }

  /**
   * List all existing locations
   */
  public async listAll(): Promise<Location[]> {
    const execution = this.database.query(
      `SELECT * FROM ${LocationRepository.tableName}`,
    );
    const result = await execution;
    return result.rows.map(this.locationFromRow);
  }

  /**
   * List all locations where owner equals given ID
   * @param id The id of the owner
   */
  public async listWhereowner(id: number): Promise<Location[]> {
    const query = {
      text: `SELECT *, ST_AsGeoJSON(geom) as geom FROM ${LocationRepository.tableName} WHERE owner = $1`,
      values: [id],
    };
    const execution = this.database.query(query);
    const result = await execution;
    return result.rows.map(this.locationFromRow);
  }

  /**
   * Return a location by its ID
   * @param id Location Id
   */
  public async findById(id: number): Promise<Location | null> {
    const query = {
      text: `SELECT *, ST_AsGeoJSON(geom) as geom FROM ${LocationRepository.tableName} WHERE id = $1`,
      values: [id],
    };
    const execution = this.database.query(query);
    const result = await execution;
    // Return Null query returned nothing
    if (result.rowCount < 1) {
      return null;
    }

    return this.locationFromRow(result.rows[0]);
  }

  /**
   * Inserts a new location into the database
   * @param location Location object
   */
  public async insert(location: Location): Promise<Location> {
    logger.info({ location }, 'Creating location');
    const query = {
      text: `INSERT INTO ${LocationRepository.tableName} (name, owner, geom) VALUES ($1, $2, ST_SetSRID(ST_MakePoint($3, $4), 4326)) RETURNING *, ST_AsGeoJSON(geom) as geom`,
      values: [
        location.name,
        location.owner,
        location.longitude, // X is longitude
        location.latitude, // Y is latitude
      ],
    };
    const execution = this.database.query(query);
    const result = await execution;
    // If nothing was inserted throw
    if (result.rowCount < 0) {
      throw new Error('Database insertion failed.');
    }

    return this.locationFromRow(result.rows[0]);
  }

  /**
   * Remove a location from the database
   * @param id Location to destroy
   */
  public async deleteById(id: number): Promise<boolean> {
    const query = {
      text: `DELETE FROM ${LocationRepository.tableName} WHERE id = $1`,
      values: [id],
    };
    const execution = this.database.query(query);
    const result = await execution;
    return result.rowCount > 0;
  }
}
