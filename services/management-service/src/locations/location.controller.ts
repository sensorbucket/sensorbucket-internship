import { Controller } from '@sensorbucket/common';
import Router from 'koa-router';
import { Context, ParameterizedContext } from 'koa';
import { LocationService } from './location.service';
import { Location } from '@sensorbucket/models';

export class LocationController extends Controller {
  private locationService: LocationService;

  constructor(locationService: LocationService) {
    super();
    this.locationService = locationService;
  }

  setupRoutes(router: Router): void {
    router.get('/', this.getAllLocations.bind(this));
    router.get('/:id', this.getLocation.bind(this));
    router.post('/', this.createLocation.bind(this));
    router.delete('/:id', this.deleteLocation.bind(this));
  }

  /**
   * Explicitly add 'geometry' property to object for JSON stringification
   * @param location The Location model to transform
   */
  locationWithGeometry(location: Location): any {
    return {
      ...location,
      geometry: location.geometry,
    };
  }

  /**
   * Return all locations this user owns
   * @apiMethod GET
   * @apiPath /locations/
   * @apiParam geometry If present, render with GeoJSON
   */
  private async getAllLocations(ctx: Context): Promise<void> {
    const asGeometry = 'geometry' in ctx.query;
    try {
      let locations = await this.locationService.getLocationsForUser(0);
      // Render with GeoJSON if requested
      if (asGeometry) {
        locations = locations.map(this.locationWithGeometry);
      }
      // Respond
      ctx.body = locations;
      ctx.status = 200;
    } catch (e) {
      ctx.status = 500;
      console.error(e);
    }
  }

  /**
   * Return Location by its ID
   * @apiMethod GET
   * @apiPath /locations/:id
   */
  private async getLocation(ctx: Context): Promise<void> {
    const asGeometry = 'geometry' in ctx.query;
    try {
      let location = await this.locationService.getLocation(ctx.params.id);
      // Not Found
      if (location === null) {
        ctx.status = 404;
        return;
      }
      // Render with GeoJSON if requested
      if (asGeometry) {
        location = this.locationWithGeometry(location);
      }
      // Respond
      ctx.body = location;
      ctx.status = 200;
    } catch (e) {
      ctx.status = 500;
      console.error(e);
    }
  }

  /**
   * Create a new location
   * @apiMethod POST
   * @apiPath /locations/
   */
  private async createLocation(ctx: ParameterizedContext): Promise<void> {
    try {
      const location = await this.locationService.createLocationForUser(
        0,
        ctx.request.body,
      );
      ctx.body = location;
      ctx.status = 200;
      return;
    } catch (e) {
      // Return proper error
      ctx.status = e.httpCode || 500;
      // If this is a user error, inform them correctly
      if (e.httpCode && e.httpCode >= 400 && e.httpCode < 500) {
        ctx.body = e;
      }
      console.error(e);
    }
  }

  /**
   * Delete a location by its id
   * @apiMethod DELETE
   * @apiPath /locations/:id/
   */
  private async deleteLocation(ctx: ParameterizedContext): Promise<void> {
    try {
      const locationId = ctx.params.id;
      // Check if user is authorized
      const deleted = await this.locationService.deleteLocation(locationId);
      ctx.status = deleted ? 200 : 404;
    } catch (e) {
      ctx.status = 500;
      console.error(e);
    }
  }
}
