import { Source } from '@sensorbucket/models';
import { logger } from '@sensorbucket/logging';
import { Client as PSQLClient, QueryResultRow } from 'pg';

export class SourceRepository {
  static readonly tableName = 'sources';

  private database: PSQLClient;

  constructor(database: PSQLClient) {
    this.database = database;
  }

  /**
   * Map a row to a Source model
   * @param row A query row representing a source
   */
  private sourceFromRow(row: QueryResultRow): Source {
    return new Source(row.id, row.name, row.owner, row.source_type);
  }

  /**
   * List all existing sources
   */
  public async listAll(): Promise<Source[]> {
    const execution = this.database.query(
      `SELECT * FROM ${SourceRepository.tableName}`,
    );
    const result = await execution;
    return result.rows.map(this.sourceFromRow);
  }

  /**
   * List all sources where owner equals given ID
   * @param id The id of the owner
   */
  public async listWhereowner(id: number): Promise<Source[]> {
    const query = {
      text: `SELECT * FROM ${SourceRepository.tableName} WHERE owner = $1`,
      values: [id],
    };
    const execution = this.database.query(query);
    const result = await execution;
    return result.rows.map(this.sourceFromRow);
  }

  /**
   * Return a source by its di
   * @param id Source Id
   */
  public async findById(id: number): Promise<Source | null> {
    const query = {
      text: `SELECT * FROM ${SourceRepository.tableName} WHERE id = $1 LIMIT 1`,
      values: [id],
    };
    const execution = this.database.query(query);
    const result = await execution;
    // Return Null query returned nothing
    if (result.rowCount < 1) {
      return null;
    }

    return this.sourceFromRow(result.rows[0]);
  }

  /**
   * Inserts a new source into the database
   * @param source Source object
   */
  public async insert(source: Source): Promise<Source> {
    const query = {
      text: `INSERT INTO ${SourceRepository.tableName} (name, owner, source_type) VALUES ($1, $2, $3) RETURNING *`,
      values: [source.name, source.owner, source.sourceType],
    };
    const execution = this.database.query(query);
    const result = await execution;
    // Throw if insert failed
    if (result.rowCount < 1) {
      throw new Error('Failed to insert new source');
    }
    return this.sourceFromRow(result.rows[0]);
  }

  /**
   * Remove a source from the database
   * @param id Source to destroy
   */
  public async deleteById(id: number): Promise<boolean> {
    const query = {
      text: `DELETE FROM ${SourceRepository.tableName} WHERE id = $1`,
      values: [id],
    };
    const execution = this.database.query(query);
    const result = await execution;
    return result.rowCount > 0;
  }
}
