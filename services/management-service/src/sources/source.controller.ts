import { Controller } from '@sensorbucket/common';
import Router from 'koa-router';
import { Context, ParameterizedContext, Middleware, Next } from 'koa';
import { SourceService } from './source.service';
import { Source } from '@sensorbucket/models';
import { ExpandMiddlewareFactory } from '@sensorbucket/resources';

export class SourceController extends Controller {
  private sourceService: SourceService;
  private expandResponse: Middleware;

  constructor(sourceService: SourceService) {
    super();
    this.sourceService = sourceService;
    this.expandResponse = ExpandMiddlewareFactory<Source>(
      sourceService.expander,
    ).bind(this);
  }

  setupRoutes(router: Router): void {
    router.get('/', this.getAllSources.bind(this), this.expandResponse);
    router.get('/:id', this.getSource.bind(this), this.expandResponse);
    router.post('/', this.createSource.bind(this), this.expandResponse);
    router.delete('/:id', this.deleteSource.bind(this));
  }

  /**
   * Return all sources this user owns
   * @apiMethod GET
   * @apiPath /sources/
   */
  private async getAllSources(ctx: Context, next: Next): Promise<void> {
    // TODO: infer user ID
    try {
      let sources = await this.sourceService.getSourcesForUser(0);
      // Result
      ctx.body = sources;
      ctx.status = 200;
      return next();
    } catch (e) {
      ctx.status = 500;
      console.error(e);
    }
  }

  private async getSource(ctx: Context, next: Next): Promise<void> {
    // TODO: infer user ID
    try {
      const source = await this.sourceService.getSource(ctx.params.id);
      if (source === null) {
        ctx.status = 404;
        return;
      }
      ctx.body = source;
      ctx.status = 200;
      return next();
    } catch (e) {
      ctx.status = 500;
      console.error(e);
    }
  }

  /**
   * Create a new source
   * @apiMethod POST
   * @apiPath /sources/
   */
  private async createSource(
    ctx: ParameterizedContext,
    next: Next,
  ): Promise<void> {
    try {
      const source = await this.sourceService.createSourceForUser(
        0,
        ctx.request.body,
      );
      ctx.body = source;
      ctx.status = 200;
      return next();
    } catch (e) {
      // Return proper error
      ctx.status = e.httpCode || 500;
      // If this is a user error, inform them correctly
      if (e.httpCode && e.httpCode >= 400 && e.httpCode < 500) {
        ctx.body = e;
      }
      console.error(e);
    }
  }

  /**
   * Delete a source by its id
   * @apiMethod DELETE
   * @apiPath /sources/:id/
   */
  private async deleteSource(
    ctx: ParameterizedContext,
    next: Next,
  ): Promise<void> {
    try {
      const sourceId = parseInt(ctx.params.id);
      // Check if user is authorized
      const deleted = await this.sourceService.deleteSource(sourceId);
      ctx.status = deleted ? 200 : 404;
      return next();
    } catch (e) {
      ctx.status = 500;
      console.error(e);
    }
  }
}
