import { SourceRepository } from './source.repository';
import { Source, SourceType } from '@sensorbucket/models';
import { pick, has, bind } from 'lodash';
import { SourceTypeService } from '../sourceTypes';
import { logger } from '@sensorbucket/logging';
import { HasPrimaryKey } from '@sensorbucket/resources';
import { SourceRelationExpander } from './source.expander';

export class SourceService implements HasPrimaryKey<Source> {
  private repo: SourceRepository;
  public readonly expander: SourceRelationExpander;

  private propertiesRequired = ['name', 'sourceType'];
  private propertiesWritable = ['name'];

  constructor(repo: SourceRepository, sourceTypeService: SourceTypeService) {
    this.repo = repo;
    this.expander = new SourceRelationExpander(sourceTypeService);
  }

  /**
   * Ensure an object is a valid Source resource.
   * This includes having the required properties
   * and having valid existing relations.
   * @param raw The raw object
   */
  private async validateSourceObject(raw: Object): Promise<string[]> {
    const errors: string[] = [];
    // Are all properties present?
    const missingProperties = this.propertiesRequired.filter(
      attr => !(attr in raw),
    );
    if (missingProperties.length > 0) {
      errors.push(
        'Missing required attributes: ' + missingProperties.join(','),
      );
    }
    // Are all not-null relations valid?
    const invalidRelations = await this.expander.validateRelations(raw);
    if (invalidRelations.length > 0) {
      errors.push('Invalid relation on: ' + invalidRelations.join(','));
    }
    return errors;
  }

  /**
   * Return all sources a user can read
   * @param user The user
   */
  getSourcesForUser(user: any): Promise<Source[]> {
    return this.repo.listWhereowner(user);
  }

  /**
   * Return a source by its ID
   * @param id The id of the source
   */
  async getSource(id: number): Promise<Source | null> {
    const source = await this.repo.findById(id);
    return source;
  }

  /**
   * Return a source by its ID
   * @param id The id of the source
   */
  get(id: number): Promise<Source | null> {
    return this.getSource(id);
  }

  /**
   * Creates a new source for a specific user
   * @param userId The User ID of the owner
   * @param sourceRaw An object representing a Source
   */
  async createSourceForUser(
    userId: number,
    sourceRaw: Object,
  ): Promise<Source> {
    // Are required properties present?
    const errors = await this.validateSourceObject(sourceRaw);
    if (errors.length > 0) {
      throw { httpCode: 400, message: 'Device is not valid', details: errors };
    }
    const source = Source.from(sourceRaw);
    source.owner = userId;
    return this.repo.insert(source);
  }

  /**
   * Delete a source by its ID
   * @param id The id of the source
   */
  deleteSource(id: number): Promise<boolean> {
    return this.repo.deleteById(id);
  }
}
