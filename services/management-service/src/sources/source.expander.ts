import { RelationExpander, FieldRelation } from '@sensorbucket/resources';
import { SourceTypeService } from '../sourceTypes';

export class SourceRelationExpander extends RelationExpander {
  constructor(private readonly sourceTypeService: SourceTypeService) {
    super();
  }

  relations = [new FieldRelation('sourceType', this.sourceTypeService)];
}
