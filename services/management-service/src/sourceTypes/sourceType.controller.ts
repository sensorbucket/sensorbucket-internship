import { Controller } from '@sensorbucket/common';
import Router from 'koa-router';
import { Context, ParameterizedContext } from 'koa';
import { SourceTypeService } from './sourceType.service';

export class SourceTypeController extends Controller {
  private service: SourceTypeService;

  constructor(sourcetypeService: SourceTypeService) {
    super();
    this.service = sourcetypeService;
  }

  setupRoutes(router: Router): void {
    router.get('/', this.getAllSourceTypes.bind(this));
    router.get('/:id', this.getSourceType.bind(this));
    router.post('/', this.createSourceType.bind(this));
    router.delete('/:id', this.deleteSourceType.bind(this));
  }

  /**
   * Return all sourcetypes this user owns
   * @apiMethod GET
   * @apiPath /sourcetypes/
   */
  private async getAllSourceTypes(ctx: Context): Promise<void> {
    // TODO: infer user ID
    try {
      ctx.body = await this.service.getSourceTypes();
      ctx.status = 200;
    } catch (e) {
      ctx.status = 500;
      console.error(e);
    }
  }

  private async getSourceType(ctx: Context): Promise<void> {
    // TODO: infer user ID
    try {
      const sourcetype = await this.service.getSourceType(ctx.params.id);
      if (sourcetype === null) {
        ctx.status = 404;
        return;
      }
      ctx.body = sourcetype;
      ctx.status = 200;
    } catch (e) {
      ctx.status = 500;
      console.error(e);
    }
  }

  /**
   * Create a new sourcetype
   * @apiMethod POST
   * @apiPath /sourcetypes/
   */
  private async createSourceType(ctx: ParameterizedContext): Promise<void> {
    try {
      const sourcetype = await this.service.createSourceType(ctx.request.body);
      ctx.body = sourcetype;
      ctx.status = 200;
      return;
    } catch (e) {
      ctx.status = 500;
      console.error(e);
    }
  }

  /**
   * Delete a sourcetype by its id
   * @apiMethod DELETE
   * @apiPath /sourcetypes/:id/
   */
  private async deleteSourceType(ctx: ParameterizedContext): Promise<void> {
    try {
      const sourcetypeId = parseInt(ctx.params.id);
      // Check if user is authorized
      const deleted = await this.service.deleteSourceType(sourcetypeId);
      ctx.status = deleted ? 200 : 404;
    } catch (e) {
      ctx.status = 500;
      console.error(e);
    }
  }
}
