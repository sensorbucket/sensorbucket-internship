import { SourceTypeRepository } from './sourceType.repository';
import { SourceType } from '@sensorbucket/models';
import { HasPrimaryKey } from '@sensorbucket/resources/src';

export class SourceTypeService implements HasPrimaryKey<SourceType> {
  private repo: SourceTypeRepository;

  private propertiesRequired = ['name'];

  constructor(repo: SourceTypeRepository) {
    this.repo = repo;
  }

  /**
   * Ensure object has all required Device attributes
   * @param raw The raw object
   */
  private validateSourceTypeObject(raw: Object): boolean {
    return this.propertiesRequired.every(attr => attr in raw);
  }

  /**
   * Return all existing Device Types
   */
  getSourceTypes(): Promise<SourceType[]> {
    return this.repo.listAll();
  }

  /**
   * Get a Device Type by its id
   * @param id The id of the Device Type
   */
  getSourceType(id: number): Promise<SourceType | null> {
    return this.repo.findById(id);
  }

  /**
   * Get a Device Type by its id
   * @param id The id of the Device Type
   */
  get(id: number): Promise<SourceType | null> {
    return this.getSourceType(id);
  }

  /**
   *
   * @param sourceType An object representing a device type
   */
  createSourceType(raw: Object): Promise<SourceType> {
    // Are required properties present?
    const valid = this.validateSourceTypeObject(raw);
    if (!valid) {
      throw new Error('Bad request');
    }
    const device = SourceType.from(raw);
    return this.repo.insert(device);
  }

  /**
   * Delete a SourceType by its ID
   * @param id The id of the SourceType
   */
  deleteSourceType(id: number): Promise<boolean> {
    return this.repo.deleteById(id);
  }
}
