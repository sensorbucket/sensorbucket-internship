import { SourceType } from '@sensorbucket/models';
import { Client as PSQLClient, QueryResultRow } from 'pg';

export class SourceTypeRepository {
  static readonly tableName = 'source_types';

  private database: PSQLClient;

  constructor(database: PSQLClient) {
    this.database = database;
  }

  /**
   * Map a row to a Device Type model
   * @param row A query row representing a Device Type
   */
  private sourceTypeFromRow(row: QueryResultRow): SourceType {
    return new SourceType(row.id, row.name, row.configuration_schema);
  }

  /**
   * List all device types
   */
  public async listAll(): Promise<SourceType[]> {
    const execution = this.database.query(
      `SELECT * FROM ${SourceTypeRepository.tableName}`,
    );
    const result = await execution;
    return result.rows.map(this.sourceTypeFromRow);
  }

  /**
   * Find a SourceType by its id
   * @param id The id of the device type
   */
  public async findById(id: number): Promise<SourceType | null> {
    const query = {
      text: `SELECT * FROM ${SourceTypeRepository.tableName} WHERE id = $1`,
      values: [id],
    };
    const execution = this.database.query(query);
    const result = await execution;
    // Return null if not found
    if (result.rowCount < 1) {
      return null;
    }
    return this.sourceTypeFromRow(result.rows[0]);
  }

  /**
   * Insert a new device into the database
   * @param sourceType SourceType object to insert
   */
  public async insert(sourceType: SourceType): Promise<SourceType> {
    const query = {
      text: `INSERT INTO ${SourceTypeRepository.tableName} (name, configuration_schema) VALUES ($1, $2) RETURNING *`,
      values: [sourceType.name, sourceType.configurationSchema],
    };
    const execution = this.database.query(query);
    const result = await execution;
    // Throw if insert failed
    if (result.rowCount < 1) {
      throw new Error('Failed to insert new SourceType');
    }
    return this.sourceTypeFromRow(result.rows[0]);
  }

  /**
   * Remove a SourceType from the database
   * @param id SourceType to remove
   */
  public async deleteById(id: number): Promise<boolean> {
    const query = {
      text: `DELETE FROM ${SourceTypeRepository.tableName} WHERE id = $1`,
      values: [id],
    };
    const execution = this.database.query(query);
    const result = await execution;
    return result.rowCount > 0;
  }
}
