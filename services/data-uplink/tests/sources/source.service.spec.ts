import { suite, test } from 'mocha-typescript';
import { expect } from 'chai';
import SourceService from '../../src/sources/source.service';
import SourceRepositoryInterface from '../../src/sources/repositories/source.repository.interface';
import { Source } from '@sensorbucket/models';

/**
 * Create a simple in-memory repository for testing
 */
class MemorySourceRepository implements SourceRepositoryInterface {
  private sources: { [index: string]: Source } = {};

  constructor(sources: { [index: string]: Source }) {
    this.sources = sources;
  }

  async getSourceByName(name: string): Promise<Source> {
    return this.sources[name];
  }
}

@suite
class SourceServiceTest {
  private sources!: { [index: string]: Source };
  private repo!: SourceRepositoryInterface;
  private service!: SourceService;

  /**
   * Before each test, reset the repository and reinstantiate the service
   */
  before() {
    this.sources = {
      TTN: new Source(1, 'TheThingsNetwork', 0, 0),
    };
    this.repo = new MemorySourceRepository(this.sources);
    this.service = new SourceService(this.repo);
  }

  /**
   * Test whether the service correctly returns true and false
   * based on if the given source by name exists.
   */
  @test
  async shouldSourceExists() {
    const exists1 = await this.service.exists('TTN');
    const exists2 = await this.service.exists('Nothing');
    expect(exists1).to.be.true;
    expect(exists2).to.be.false;
  }
}
