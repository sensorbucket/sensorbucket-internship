import { suite, test } from 'mocha-typescript';
import { expect } from 'chai';
import { instance, mock, verify, capture, anything } from 'ts-mockito';
import { MQProducerInterface } from '@sensorbucket/message-queue';
import UplinkService from '../../src/uplink/uplink.service';

@suite
class UplinkServiceTest {
  MQMock!: MQProducerInterface;
  queue!: MQProducerInterface;
  service!: UplinkService;

  before() {
    this.MQMock = mock<MQProducerInterface>();
    this.queue = instance(this.MQMock);
    this.service = new UplinkService(this.queue);
  }

  /**
   * The newUplink method should return a guid
   */
  @test
  async shouldReturnGUID() {
    const response = await this.service.newUplink({
      headers: {},
      body: '',
      source: '',
      deviceType: '',
    });
    expect(response).to.have.property('guid');
  }

  /**
   * The newUplink method should return proper guids (unique!)
   *
   * This test is mostly meant to make sure the method does not return exactly
   * the same guids every time.
   */
  @test
  async shouldNotReturnDuplicateGUID() {
    const emptyUplink = {
      headers: {},
      body: '',
      source: '',
      deviceType: '',
    };
    const requests = [
      this.service.newUplink(emptyUplink),
      this.service.newUplink(emptyUplink),
      this.service.newUplink(emptyUplink),
      this.service.newUplink(emptyUplink),
      this.service.newUplink(emptyUplink),
    ];

    /*
      A Set object does not allow duplicates
    */
    const guids = (await Promise.all(requests)).map(obj => obj.guid);
    const guidsSet = new Set(guids);
    expect(guids.length).to.equal(
      guidsSet.size,
      'Duplicate guids in a set of 5',
    );
  }

  /**
   * Ensure that produce on MessageQueue is called with given requestData
   */
  @test
  async shouldProduceMessage() {
    const uplinkDTO = await this.service.newUplink({
      headers: {},
      body: '',
      source: 'testSource',
      deviceType: '',
    });
    // Verify that produce was called with anything atleast once
    verify(this.MQMock.produce('testSource', anything())).called();
    // Make sure that the queue has the same dto as we got back in uplinkDTO
    const [_, queueMessage] = capture(this.MQMock.produce).last();
    expect(queueMessage).to.deep.equal(
      uplinkDTO,
      'Service returns different object than passed to queue!',
    );
  }
}
