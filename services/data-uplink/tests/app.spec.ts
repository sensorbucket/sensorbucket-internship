import { suite, test } from 'mocha-typescript';
import { expect } from 'chai';

@suite
class AppTest {
  @test
  shouldAlwaysSucceed() {
    expect(1 + 1).to.equal(2);
  }
}
