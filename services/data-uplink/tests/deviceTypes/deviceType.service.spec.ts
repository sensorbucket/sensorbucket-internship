import { suite, test } from 'mocha-typescript';
import { expect } from 'chai';
import DeviceTypeService from '../../src/deviceTypes/deviceType.service';
import DeviceTypeRepositoryInterface from '../../src/deviceTypes/repositories/deviceType.repository.interface';
import { DeviceType } from '@sensorbucket/models';

/**
 * Create a simple in-memory repository for testing
 */
class MemoryDeviceTypeRepository implements DeviceTypeRepositoryInterface {
  private deviceTypes: { [index: string]: DeviceType } = {};

  constructor(deviceTypes: { [index: string]: DeviceType }) {
    this.deviceTypes = deviceTypes;
  }

  async getDeviceTypeByName(name: string): Promise<DeviceType> {
    return this.deviceTypes[name];
  }
}

@suite
class DeviceTypeServiceTest {
  private deviceTypes!: { [index: string]: DeviceType };
  private repo!: DeviceTypeRepositoryInterface;
  private service!: DeviceTypeService;

  /**
   * Before each test, reset the repository and reinstantiate the service
   */
  before() {
    this.deviceTypes = {
      MFM: new DeviceType(1, 'Multiflexmeter', '0.0.1-test', {}),
    };
    this.repo = new MemoryDeviceTypeRepository(this.deviceTypes);
    this.service = new DeviceTypeService(this.repo);
  }

  /**
   * Test whether the service correctly returns true and false
   * based on if the given deviceType by name exists.
   */
  @test
  async shouldDeviceTypeExists() {
    const exists1 = await this.service.exists('MFM');
    const exists2 = await this.service.exists('Nothing');
    expect(exists1).to.be.true;
    expect(exists2).to.be.false;
  }
}
