/**
 * This is the format the configuration has to adhere to
 */
interface DeviceUplinkServiceConfigInterface {
  HTTP_PORT: number;
  AMQP_URI: string;
}

export default class Config {
  private static configMap: DeviceUplinkServiceConfigInterface = {
    HTTP_PORT: 3000,
    AMQP_URI: 'amqp://localhost:5672/',
  };

  /**
   * Get the configuration object
   */
  static get(): DeviceUplinkServiceConfigInterface {
    return this.configMap;
  }

  /**
   * Load a configuration file
   *
   * Any options not specified will be set to their defaults.
   *
   * @param path Path to the configuration file
   */
  static async loadConfig(filePath: string): Promise<void> {
    const config: DeviceUplinkServiceConfigInterface = require(filePath);
    // TODO: could allow for arbitrary data to be inserted. (Keys that the interface does not specify)
    // Override defaults
    this.configMap = {
      ...this.configMap,
      ...config,
    };
  }
}
