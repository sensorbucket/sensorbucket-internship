import Koa, { Context, Next } from 'koa';
import Router from 'koa-router';
import bodyParser from 'koa-bodyparser';

import Config from './config';
import UplinkController from './uplink/uplink.controller';
import { Controller } from '@sensorbucket/common';
import { AMQPConnection, AMQPChannel } from '@sensorbucket/mq-amqp';
import { logger } from '@sensorbucket/logging';
import UplinkService from './uplink/uplink.service';
import DeviceTypeService from './deviceTypes/deviceType.service';
import SourceService from './sources/source.service';
import DeviceTypeRepositoryInterface from './deviceTypes/repositories/deviceType.repository.interface';
import SourceRepositoryInterface from './sources/repositories/source.repository.interface';
import { MQProducerInterface } from '@sensorbucket/message-queue';
import DeviceTypeInMemoryRepository from './deviceTypes/repositories/deviceType.memory.repository';
import SourceInMemoryRepository from './sources/repositories/source.memory.repository';
import { DeviceType } from '@sensorbucket/models';
import { Source } from '@sensorbucket/models';

export default class DataUplinkService {
  koaServer: Koa;
  koaRouter: Router;
  mqConnection!: AMQPConnection;
  mqChannel!: AMQPChannel;

  // TODO: Is this the best approach?
  // Repositories
  deviceTypeRepository!: DeviceTypeRepositoryInterface;
  sourceRepository!: SourceRepositoryInterface;

  // TODO: Is this the best approach?
  // Services
  deviceTypeService!: DeviceTypeService;
  sourceService!: SourceService;
  uplinkService!: UplinkService;

  constructor() {
    this.koaServer = new Koa();
    this.koaRouter = new Router();
  }

  /**
   * Try to load the configuration file
   */
  async loadConfig(): Promise<void> {
    try {
      await Config.loadConfig('../config.json');
    } catch (error) {
      if (error.code === 'MODULE_NOT_FOUND') {
        logger.warn('Configuration file not found, using defaults.');
      }
    }
  }

  /**
   * Register a controller to an URL Path
   * @param path The URL Path the controller owns
   * @param controller The controller instance
   */
  registerController(path: string, controller: Controller) {
    const router = new Router();
    controller.setupRoutes(router);
    // Omit path if empty or root to avoid path with double slash
    if (path === '/' || path === '' || path === undefined || path === null) {
      this.koaRouter.use(router.routes());
    } else {
      this.koaRouter.use(path, router.routes());
    }
  }

  /**
   * Instantiate and configure all repositories
   */
  async loadRepositories() {
    this.deviceTypeRepository = new DeviceTypeInMemoryRepository({
      mfm: new DeviceType(1, 'Multiflexmeter', '0.0.1-placeholder', {}),
    });
    this.sourceRepository = new SourceInMemoryRepository({
      ttn: new Source(1, 'TheThingsNetwork', 0, 0),
    });
  }

  /**
   * Instantiate and configure all services
   */
  async loadServices() {
    this.deviceTypeService = new DeviceTypeService(this.deviceTypeRepository);
    this.sourceService = new SourceService(this.sourceRepository);
    this.uplinkService = new UplinkService(this.mqChannel);
  }

  /**
   * Load and initialize all controllers
   */
  async loadControllers() {
    const uplinkController = new UplinkController(
      this.uplinkService,
      this.sourceService,
      this.deviceTypeService,
    );
    this.registerController('/', uplinkController);
    this.koaServer.use(this.koaRouter.routes());
  }

  /**
   * Promisify koa listening
   *
   * Resolves when listener is started.
   */
  async startKoaServer() {
    return new Promise(resolve => {
      this.koaServer.listen(Config.get().HTTP_PORT, resolve);
    });
  }

  async connectToMQ(): Promise<void> {
    this.mqConnection = new AMQPConnection(Config.get().AMQP_URI);
    await this.mqConnection.connect();
    this.mqChannel = await this.mqConnection.openChannel();
  }

  /**
   * Start this service.
   */
  async start() {
    this.koaServer.use(bodyParser());
    await this.loadConfig();
    await this.connectToMQ();
    await this.loadRepositories();
    await this.loadServices();
    await this.loadControllers();
    await this.startKoaServer();
  }
}

const app = new DataUplinkService();
app
  .start()
  .then(() => {
    logger.info(
      { eventCode: 'service.data-uplink.listening' },
      `HTTP Server listening on ${Config.get().HTTP_PORT}`,
    );
  })
  .catch(logger.error.bind(logger));
