import SourceRepositoryInterface from './source.repository.interface';
import { Source } from '@sensorbucket/models';

export default class SourceInMemoryRepository
  implements SourceRepositoryInterface {
  constructor(private sources: { [index: string]: Source }) {}

  async getSourceByName(name: string): Promise<Source> {
    return this.sources[name];
  }
}
