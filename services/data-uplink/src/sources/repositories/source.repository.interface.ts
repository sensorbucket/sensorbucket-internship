import { Source } from '@sensorbucket/models';

export default interface SourceRepositoryInterface {
  /**
   * Return a source by its name
   * @param name Source name
   */
  getSourceByName(name: string): Promise<Source>;
}
