import SourceRepositoryInterface from './repositories/source.repository.interface';

export default class SourceService {
  private repository: SourceRepositoryInterface;

  constructor(repository: SourceRepositoryInterface) {
    this.repository = repository;
  }

  /**
   * Check if a Source with the given name exists
   * @param name The Source name
   */
  async exists(name: string): Promise<boolean> {
    const source = await this.repository.getSourceByName(name);
    return source !== undefined && source !== null;
  }
}
