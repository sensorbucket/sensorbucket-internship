import UUID from 'uuid/v4';
import { MQProducerInterface } from '@sensorbucket/message-queue';
import { GUIDInterface, HTTPRequestDataInterface } from '@sensorbucket/common';
import { logger } from '@sensorbucket/logging';

/**
 * Service that is responsible for Uplink communication
 */
export default class UplinkService {
  constructor(private mqProducer: MQProducerInterface) {}

  /**
   * Creates a new uplink DTO and pushes it to the queue
   * @param request The HTTP Request data
   */
  async newUplink(request: HTTPRequestDataInterface): Promise<GUIDInterface> {
    // Create dto with raw-request and new guid according to relevant interfaces
    const dto: HTTPRequestDataInterface & GUIDInterface = {
      ...request,
      guid: this.generateUUID(),
    };
    //
    logger.info(
      { eventCode: 'service.data-uplink.uplink-service.newUplink', dto },
      'Created a new uplink DTO.',
    );
    // Push dto to queue
    try {
      await this.mqProducer.produce(dto.source, dto);
    } catch (e) {
      throw new Error(`Could not produce to queue, does it exist? E:${e.code}`);
    }
    // Return dto
    return dto;
  }

  /**
   * Generates a random UUID
   */
  generateUUID() {
    return UUID();
  }
}
