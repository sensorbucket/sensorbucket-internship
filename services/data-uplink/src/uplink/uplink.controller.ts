import Router from 'koa-router';
import { Context } from 'koa';
import { Controller } from '@sensorbucket/common';
import DeviceTypeService from '../deviceTypes/deviceType.service';
import SourceService from '../sources/source.service';
import UplinkService from './uplink.service';

/**
 * Controller for posting uplink messages
 */
export default class UplinkController extends Controller {
  constructor(
    private uplinkService: UplinkService,
    private sourceService: SourceService,
    private deviceTypeService: DeviceTypeService,
  ) {
    super();
  }

  setupRoutes(router: Router): void {
    router.post('/:source/:deviceType', this.postUplink.bind(this));
  }

  /**
   * POST to start a new uplink process for given Source and DeviceType.
   */
  async postUplink(ctx: Context & Router.IRouterParamContext) {
    const { source, deviceType } = ctx.params;
    // Check if Source exists
    if (!(await this.sourceService.exists(source))) {
      ctx.response.status = 404;
      ctx.body = { message: 'Source does not exist or is disabled' };
      return;
    }
    // Check if DeviceType exists
    if (!(await this.deviceTypeService.exists(deviceType))) {
      ctx.response.status = 404;
      ctx.body = { message: 'DeviceType does not exist or is disabled' };
      return;
    }
    // Create new Uplink DTO
    const dto = await this.uplinkService.newUplink({
      headers: ctx.request.headers,
      body: ctx.request.body,
      source,
      deviceType,
    });
    // TODO: Create dto status url
    const statusURL = `${ctx.protocol}://${ctx.hostname}/api/uplink/${dto.guid}`;
    // Respond
    ctx.body = { status: statusURL };
    ctx.status = 202;
  }
}
