import DeviceTypeRepositoryInterface from './deviceType.repository.interface';
import { DeviceType } from '@sensorbucket/models';

export default class DeviceTypeInMemoryRepository
  implements DeviceTypeRepositoryInterface {
  constructor(private sources: { [index: string]: DeviceType }) {}

  async getDeviceTypeByName(name: string): Promise<DeviceType> {
    return this.sources[name];
  }
}
