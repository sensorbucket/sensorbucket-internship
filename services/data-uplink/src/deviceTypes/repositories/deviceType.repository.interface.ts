import { DeviceType } from '@sensorbucket/models';

export default interface DeviceTypeRepositoryInterface {
  getDeviceTypeByName(name: string): Promise<DeviceType>;
}
