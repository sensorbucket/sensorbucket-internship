import DeviceTypeRepositoryInterface from './repositories/deviceType.repository.interface';

export default class DeviceTypeService {
  private repository: DeviceTypeRepositoryInterface;

  constructor(repositry: DeviceTypeRepositoryInterface) {
    this.repository = repositry;
  }

  /**
   * Check if a DeviceType with the given name exists
   * @param name The DeviceType name
   */
  async exists(name: string): Promise<boolean> {
    const source = await this.repository.getDeviceTypeByName(name);
    return source !== undefined && source !== null;
  }
}
