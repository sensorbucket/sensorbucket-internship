import { logger } from '@sensorbucket/logging';
import Express from 'express';
import proxy from 'http-proxy-middleware';

class ApiGateway {
  private express: Express.Application;

  constructor() {
    this.express = Express();
  }

  /**
   * Register system services
   */
  private registerServices(): void {
    const express = this.express;
    // Proxy uplink
    express.use(
      proxy('/api/v1/data', {
        target: 'http://data-uplink:3000',
        pathRewrite: {
          '^/api/v1/data': '',
        },
      }),
    );
    // Proxy management-service
    express.use(
      proxy('/api/v1/(devices|locations|sources|sourcetypes|devicetypes)', {
        target: 'http://management-service:3000',
        pathRewrite: {
          '^/api/v1': '',
        },
      }),
    );
  }

  async start() {
    // Register services
    this.registerServices();
    // Start listening and wait for it
    await new Promise(resolve => {
      this.express.listen(3000, resolve);
    });
  }
}

const apiGateway = new ApiGateway();
apiGateway.start().then(() => {
  logger.info(
    { eventCode: 'service.api-gateway.started' },
    'API-Gateway started',
  );
});
