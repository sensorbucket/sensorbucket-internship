const { partial, every, has } = require('lodash');

/*
  The properties the DTO must have
 */
const requiredProperties = ['body.payload_raw', 'body.metadata.time'];

/**
 * Validates the request
 */
const validateRequest = dto => {
  // Check if for every requiredProperty, dto has the property
  const valid = every(requiredProperties, partial(has, dto));
  if (!valid) {
    throw { code: 'INVALID_PAYLOAD' };
  }
};

module.exports = {
  // Called once
  setup: async logger => {
    this.logger = logger;
    logger.info(
      { eventCode: 'worker.source.script.thethingsnetwork.setup.success' },
      'TheThingsNetwork script succesfully loaded',
    );
  },
  // Do conversion
  work: async dto => {
    validateRequest(dto);
    return {
      ...dto,
      deviceData: dto.body.payload_raw,
      timestamp: dto.body.metadata.time,
    };
  },
};
