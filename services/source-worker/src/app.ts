import { Config } from './config';
import { MQMessage } from '@sensorbucket/message-queue';
import { AMQPConnection, AMQPChannel } from '@sensorbucket/mq-amqp';
import { logger } from '@sensorbucket/logging';
import { HTTPRequestDataInterface } from '@sensorbucket/common/src';

interface WorkerScript {
  setup(logger: any): Promise<void>;
  work(dto: HTTPRequestDataInterface): Promise<any>;
}

class WorkerBootstrap {
  private connection!: AMQPConnection;
  private worker!: WorkerScript;
  private queueChannel!: AMQPChannel;

  /**
   * Connect to the AMQP Server
   */
  private async connectToQueue(): Promise<void> {
    try {
      this.connection = new AMQPConnection(Config.get().AMQP_URI);
      await this.connection.connect();
      this.queueChannel = await this.connection.openChannel();
    } catch (e) {
      logger.error(
        { eventCode: 'worker.source.connectToQueue.error', error: e },
        'Could not connect to AMQP Server! Exiting...',
      );
      process.exit(1);
    }
  }

  /**
   * Check if config is valid
   */
  private validateConfig(): void {
    // Ensure QUEUE_NAME and WORKER are set
    const queueName = Config.get().QUEUE_NAME;
    const workerName = Config.get().WORKER;
    if (
      queueName === 'QUEUE_NOT_SPECIFIED' ||
      workerName === 'WORKER_NOT_SPECIFIED'
    ) {
      logger.error({}, 'Missing QUEUE_NAME or WORKER in config file');
      process.exit(1);
    }
  }

  /**
   * Load ../config.json if exists
   */
  private async loadConfig(): Promise<void> {
    try {
      await Config.loadConfig('../config.json');
      this.validateConfig();
    } catch (e) {
      // Let the user know the configuration is reverting to defaults
      if (e.code === 'MODULE_NOT_FOUND') {
        logger.warn(
          { eventCode: 'worker.source.loadConfig.error' },
          'Could not load ../config.json, using defaults',
        );
      }
    }
  }

  private async loadWorker() {
    try {
      const workerName = Config.get().WORKER;
      // Load worker script
      this.worker = require(`../scripts/${workerName}`);
      // Call setup
      await this.worker.setup(logger);
      // Throw error if no script was found
    } catch (e) {
      if (e.code === 'MODULE_NOT_FOUND') {
        logger.error(
          { eventCode: 'worker.device.loadWorker.error' },
          'Worker package not found',
        );
        process.exit(1);
      } else {
        logger.error(e);
      }
    }
  }

  /**
   * Get the queuename for the given device
   * @param deviceType
   */
  private queueForDevice(deviceType: string): string {
    return deviceType;
  }

  /**
   *
   * @param message Received message from the MQ
   */
  private async onMessage(message: MQMessage): Promise<any> {
    const dto = message.body as HTTPRequestDataInterface;
    // Try to parse message
    try {
      const result = await this.worker.work(dto);
      logger.info(
        { eventCode: 'worker.source.result', result },
        'Finished Job',
      );
      // All good, produce and acknowledge message
      this.queueChannel.produce(this.queueForDevice(dto.deviceType), result);
      message.acknowledge();
    } catch (e) {
      // If payload was invalid, (n)ack message to avoid loop
      // TODO: nack Message!
      if (e.code === 'INVALID_PAYLOAD') {
        message.acknowledge();
      }
      logger.error(
        { eventCode: 'worker.source.script.error', error: e },
        'Source worker script error',
      );
    }
  }

  /**
   * Start listening to the queue
   */
  private async startListening(): Promise<void> {
    this.queueChannel.addListener(
      Config.get().QUEUE_NAME,
      this.onMessage.bind(this),
    );
  }

  /**
   * Start the worker
   */
  async start(): Promise<void> {
    await this.loadConfig();
    await this.connectToQueue();
    await this.loadWorker();
    await this.startListening();
  }
}

new WorkerBootstrap().start().then(() => {
  logger.info({ eventCode: 'worker.source.started' }, 'Source Worker started');
});
