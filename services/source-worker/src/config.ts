import * as amqp from 'amqplib';

/**
 * This is the format the configuration has to adhere to
 */
interface TTNWorkerConfigInterface {
  AMQP_URI: string;
  AMQP_ASSERT_QUEUE_OPTIONS?: amqp.Options.AssertQueue;
  AMQP_CONSUME_OPTIONS?: amqp.Options.Consume;
  QUEUE_NAME: string;
  WORKER: string;
}

export class Config {
  // Default configuration
  private static configMap: TTNWorkerConfigInterface = {
    AMQP_URI: 'amqp://localhost:5672/',
    AMQP_ASSERT_QUEUE_OPTIONS: {
      durable: true,
    },
    AMQP_CONSUME_OPTIONS: {
      noAck: false,
    },
    QUEUE_NAME: 'QUEUE_NOT_SPECIFIED',
    WORKER: 'WORKER_NOT_SPECIFIED',
  };

  /**
   * Get the configuration object
   */
  static get(): TTNWorkerConfigInterface {
    return this.configMap;
  }

  /**
   * Load a configuration file
   *
   * Any options not specified will be set to their defaults.
   *
   * @param path Path to the configuration file
   */
  static async loadConfig(filePath: string): Promise<void> {
    const config: TTNWorkerConfigInterface = require(filePath);
    // TODO: could allow for arbitrary data to be inserted. (Keys that the interface does not specify)
    // Override defaults
    this.configMap = {
      ...this.configMap,
      ...config,
    };
  }
}
