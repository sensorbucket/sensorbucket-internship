/**
 * Describes what fields to expand
 */
export class ExpandMeta {
  constructor(
    public readonly resourceName: string,
    public resourceFields: ExpandMeta[],
  ) {}

  /**
   * Create ExpandMeta from an array of field paths.
   *
   * The given field paths can be nested
   * Example:
   * 'source'             // Reference source relation
   * 'source.type'        // also reference 'type' relation in 'source' relation
   * 'source.type.owner'  // also reference 'owner' relation in 'type' relation in 'source' relation
   * @param paths An array of field paths
   */
  static from(paths: string[]): ExpandMeta[] {
    // Explode
    const exploded = paths.map(p => p.split('.'));
    return ExpandMeta.fromExploded(exploded);
  }

  /**
   * Create ExpandMeta from an array of field paths nested as array.
   *
   * The given field paths can be nested
   * Example:
   * ['source' ]            // Reference source relation
   * ['source', 'type']        // also reference 'type' relation in 'source' relation
   * ['source', 'type', 'owner']  // also reference 'owner' relation in 'type' relation in 'source' relation
   * @param explodedPaths An array of field paths as array (see example)
   */
  static fromExploded(explodedPaths: string[][]): ExpandMeta[] {
    const expandMeta: ExpandMeta[] = [];
    // Create a set of every first element (TLRs)
    const topLevelResources = new Set(explodedPaths.map(e => e[0]));
    // Loop TLRs
    topLevelResources.forEach(tlr => {
      /*
       Filter paths that need expandMeta for this tlr.
       That means first part must equal tlr
       and it need atleast a children (length > 1 , first item is tlr itself)
      */
      const tlrPaths = explodedPaths
        .filter(path => path[0] === tlr && path.length > 1)
        .map(path => path.splice(1));
      // Create array for possible expandMeta of nested paths
      let tlrExpandMeta: ExpandMeta[] = [];
      // Only recurse if tlr has nested paths
      if (tlrPaths.length > 0) {
        tlrExpandMeta = ExpandMeta.fromExploded(tlrPaths);
      }
      // Push tlr expandMeta
      expandMeta.push(new ExpandMeta(tlr, tlrExpandMeta));
    });
    return expandMeta;
  }
}
