/**
 * Indicates the resource can be fetched using a Primary Key (PK)
 */
export interface HasPrimaryKey<T> {
  get(pk: any): Promise<T | null>;
}
