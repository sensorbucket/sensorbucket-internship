import { RelationExpander } from './relationExpander.abstract';
import { ParameterizedContext, Next, Middleware } from 'koa';

/**
 * Creates middleware to expand a single or array of resource.
 * The resulting middleware is valid Koa Middleware.
 * @param expander The RelationExpander for this Resource
 */
export const ExpandMiddlewareFactory = <Resource>(
  expander: RelationExpander,
): Middleware => {
  // Return actual middleware
  return async (ctx: ParameterizedContext, next: Next) => {
    // Explode path query into array (split by commas)
    const expandQuery = ctx.query.expand;
    const expandFields = (expandQuery && expandQuery.split(',')) || [];
    // Decide whether result is one Device or multiple
    if (Array.isArray(ctx.body)) {
      // Expand array of devices
      const devices = ctx.body as Resource[];
      const concurrencies = devices.map(device =>
        expander.expand(device, expandFields),
      );
      ctx.body = await Promise.all(concurrencies);
    } else {
      // Expand single device
      const device = ctx.body as Resource;
      ctx.body = await expander.expand(device, expandFields);
    }
    return next();
  };
};
