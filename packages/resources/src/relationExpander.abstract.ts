import { HasPrimaryKey } from './primarykey.interface';
import { ExpandMeta } from './expandMeta';

/**
 * Represents an object its field and corresponding value
 */
class Field {
  constructor(public readonly name: string, public value: any) {}
}

/**
 * Describes the relation between a field and a resource
 */
export class FieldRelation<T> {
  /**
   * Describes the relation between a field and a resource
   * @param fieldName The field name with the foreignKey
   * @param resolver An object that can resolve the resource by its PK
   * @param expander The resources expander implementation
   * @generic {T} Indicates the resource model
   */
  constructor(
    public readonly fieldName: string,
    public readonly resolver: HasPrimaryKey<T>,
    public readonly expander: RelationExpander | null = null,
  ) {}
}

/**
 * Temporary type definition for a Resource model
 */
type Resource = { [index: string]: any };

export abstract class RelationExpander {
  /**
   * Defines what fields are resolvable by which Resource implementing a PK
   */
  abstract readonly relations: FieldRelation<Resource>[];

  /**
   * Expand fields on a given resource by paths given as strings
   * Example for paths
   * 'source'
   * 'source.owner'
   * 'source.type.owner'
   *
   * @param resource The resource to expand
   * @param paths The field paths to expand
   */
  expand(resource: Resource, paths: string[]): Promise<Resource> {
    const expandMetas = ExpandMeta.from(paths);
    return this.expandByMeta(resource, expandMetas);
  }

  /**
   * Expand fields on a given resource by pre-defined expand meta data
   * @param resource The resource to expand
   * @param expandMetas ExpandMeta array
   */
  async expandByMeta(
    resource: Resource,
    expandMetas: ExpandMeta[],
  ): Promise<Resource> {
    // Expand every given path
    const expandPromises = expandMetas.map(expandMeta =>
      this.expandRelation(resource, expandMeta),
    );
    // Await expanding of relations, returns Field
    const newFields = await Promise.all(expandPromises);
    newFields.forEach(field => {
      resource[field.name] = field.value;
    });
    return resource;
  }

  /**
   * Validate defined relations of a resource.
   * If a field with a relation is not null, then it will be validated.
   * Returns a list of fields that have an invalid relation.
   * Thus, if an empty array is returned then the object has only valid relations.
   *
   * @param resource The resource to validate
   */
  public async validateRelations(resource: Resource): Promise<string[]> {
    // Pre-define list of failed fields
    const invalidRelations: string[] = [];
    // Expand everything. If an expansion === null then return false
    const expandResultPromises = this.relations.map(async relation => {
      // Try to resolve the field
      const relationField = relation.fieldName;
      const pk = resource[relationField];
      // If no relation is specified then continue
      if (pk === null || pk === undefined) return;
      const related = await relation.resolver.get(pk);
      // if relation is invalid then add the field in question to the invalidFields array
      if (related === null) {
        invalidRelations.push(relationField);
      }
    });
    // Wait for all to resolve
    await Promise.all(expandResultPromises);
    // Return failed fields
    return invalidRelations;
  }

  /**
   * Expand a relation given by expandMeta on a resource
   * @param resource The resource to expand a relation on
   * @param expandMeta The expand meta information
   */
  private async expandRelation(
    resource: Resource,
    expandMeta: ExpandMeta,
  ): Promise<Field> {
    const { resourceName } = expandMeta;
    // Current field value
    const field = new Field(resourceName, resource[resourceName]);
    const relation = this.getFieldRelation(resourceName);
    // If no relation is specified or no value is specified then return unmodified field
    if (
      relation === null ||
      field.value === null ||
      field.value === undefined
    ) {
      return field;
    }
    // Resolve relation and return modified field
    const relatedResource = await relation.resolver.get(field.value);
    // Throw if relation is broken
    if (relatedResource === null) {
      throw new Error(
        `Cannot resolve resource ${resourceName}, it does not exist!`,
      );
    }
    // Expand relatedResource if necessary
    if (expandMeta.resourceFields.length > 0 && relation.expander !== null) {
      // Expand and assign new value
      field.value = await relation.expander.expandByMeta(
        relatedResource,
        expandMeta.resourceFields,
      );
    } else {
      field.value = relatedResource;
    }
    return field;
  }

  /**
   * Returns the relation of a given fieldName
   * @param fieldName The name of the field
   */
  private getFieldRelation(fieldName: string): FieldRelation<any> | null {
    return (
      this.relations.find(relation => relation.fieldName === fieldName) || null
    );
  }
}
