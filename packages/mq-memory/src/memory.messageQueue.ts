import {
  MQProducerInterface,
  MQListenerInterface,
  MQListenerFunc,
  MQMessage,
} from '@sensorbucket/message-queue';
import { EventEmitter } from 'events';

export class InMemoryMessageQueueClient
  implements MQProducerInterface, MQListenerInterface {
  /**
   * A key-value object that provides a mapping: queueName to MemoryQueueClient instances
   */
  private static instance: InMemoryMessageQueueClient;

  /**
   * Returns or creates an instance for the given queue name
   * @param queueName The name of the queue
   */
  static getInstance(): InMemoryMessageQueueClient {
    return this.instance || new InMemoryMessageQueueClient();
  }

  /**
   * Returns or creates an instance for the given queue name
   * @param queueName The name of the queue
   */
  static connect(): InMemoryMessageQueueClient {
    return this.getInstance();
  }

  private listeners: {
    [index: string]: { [index: string]: MQListenerFunc };
  } = {};
  private emitters: { [index: string]: EventEmitter } = {};
  private consumerCount: number = 0;
  private messageCount = 0;

  constructor() {
    InMemoryMessageQueueClient.instance = this;
  }

  async addListener(queue: string, listener: MQListenerFunc): Promise<string> {
    // Create queue or event emitter if it doesn't exist
    this.listeners[queue] = this.listeners[queue] || {};
    this.emitters[queue] = this.emitters[queue] || new EventEmitter();
    // Register listener
    const tag = this.consumerCount++;
    this.listeners[queue][tag] = listener;
    this.emitters[queue].on('message', listener);
    return tag.toString();
  }

  async removeListener(queue: string, listenerTag: string): Promise<void> {
    if (this.listeners[queue] === undefined) return;
    const listener = this.listeners[queue][listenerTag];
    const emitter = this.emitters[queue].removeListener('message', listener);
  }

  async produce(queue: string, body: any): Promise<void> {
    const message: MQMessage = {
      id: this.messageCount++,
      body,
      acknowledge: async () => {},
    };
    if (this.emitters[queue] !== undefined && this.emitters[queue] !== null)
      this.emitters[queue].emit('message', message);
  }

  async close(): Promise<void> {
    delete InMemoryMessageQueueClient.instance;
  }
}
