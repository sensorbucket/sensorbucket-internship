import { suite, test } from 'mocha-typescript';
import { expect } from 'chai';
import { mock, instance, verify, anything, capture } from 'ts-mockito';
import { InMemoryMessageQueueClient } from '../src/memory.messageQueue';
import { MQMessage } from '@sensorbucket/message-queue';

class ListenerMock {
  async listenerFunction(message: MQMessage): Promise<void> {}
}

@suite
class InMemoryQueueTest {
  queue!: InMemoryMessageQueueClient;
  listenerMock!: ListenerMock;

  before() {
    this.queue = InMemoryMessageQueueClient.connect();
    this.listenerMock = mock(ListenerMock);
  }

  after() {
    InMemoryMessageQueueClient.getInstance().close();
  }

  @test
  should_call_listeners_on_produce() {
    const listenerInstance = instance(this.listenerMock);
    const body = 'test-message';
    this.queue.addListener('test-queue', listenerInstance.listenerFunction);
    // Produce something
    this.queue.produce('test-queue', body);
    // Verify listener has been called exactly once
    verify(this.listenerMock.listenerFunction(anything())).once();
  }

  @test
  should_call_listeners_with_body_on_produce() {
    const listenerInstance = instance(this.listenerMock);
    const body = 'test-message';
    this.queue.addListener('test-queue', listenerInstance.listenerFunction);
    // Produce something
    this.queue.produce('test-queue', body);
    // Capture the first parameter passed of the last call to the listenerFunction
    const [message] = capture(this.listenerMock.listenerFunction).last();
    expect(message.body).to.equal(body);
  }

  @test
  async should_return_listener_tag() {
    const listenerInstance = instance(this.listenerMock);
    const tag = await this.queue.addListener(
      'test-queue',
      listenerInstance.listenerFunction,
    );
    expect(tag).to.be.a('string');
  }

  @test
  async should_not_call_on_removed_listener() {
    const listenerInstance = instance(this.listenerMock);
    const tag = await this.queue.addListener(
      'test-queue',
      listenerInstance.listenerFunction,
    );
    await this.queue.removeListener('test-queue', tag);
    verify(this.listenerMock.listenerFunction(anything())).never();
  }
}
