/**
 * Allows to produce message to the queue
 */
export interface MQProducerInterface {
  /**
   * Queue a message
   * @param message The message to queue
   */
  produce(queue: string, message: any): Promise<void>;

  /**
   * Close the queue connection
   */
  close(): Promise<void>;
}
