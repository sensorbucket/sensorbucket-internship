import { MQMessage } from './message.interface';

/**
 * Signature for a listener function
 */
export type MQListenerFunc = (message: MQMessage) => Promise<any>;

/**
 * Allows listeners to be added to this queue
 */
export interface MQListenerInterface {
  /**
   * Adds a new listener to this queue
   * @param listener The listener to add
   */
  addListener(queue: string, listener: MQListenerFunc): Promise<string>;

  /**
   * Removes a listener from this queue
   * @param listener The listener to remove
   */
  removeListener(queue: string, listenerTag: string): Promise<void>;

  /**
   * Close the queue connection
   */
  close(): Promise<void>;
}
