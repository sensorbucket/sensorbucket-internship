/**
 * A generic Message Queue message
 */
export interface MQMessage {
  id: any;
  body: any;
  acknowledge(): Promise<void>;
}
