import Router from 'koa-router';
import { logger } from '@sensorbucket/logging';

/**
 * A controller registers API Resource Paths to methods which uses services to fulfill the request
 */
export abstract class Controller {
  /**
   * Bind paths to the controller methods here
   * @param router This controller's router instance
   */
  abstract setupRoutes(router: Router): void;
}
