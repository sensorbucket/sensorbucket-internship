/**
 * Timestamp property
 */
export interface TimestampInterface {
  /** ISO8601 formatted timestamp */
  timestamp: string;
}
