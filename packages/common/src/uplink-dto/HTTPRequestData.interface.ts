/**
 * Interface that describes a generic HTTP request
 */
export interface HTTPRequestDataInterface {
  headers: { [index: string]: string };
  body: any;
  source: string;
  deviceType: string;
}
