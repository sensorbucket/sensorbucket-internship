/**
 * Device specific data
 *
 * This is the data a specific device type can produce or consume
 */
export interface DeviceDataInterface {
  /**
   * Device Data is a base64 representation of a byte buffer
   */
  deviceData: string;
}
