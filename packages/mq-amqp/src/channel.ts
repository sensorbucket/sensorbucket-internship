import * as amqp from 'amqplib';
import {
  MQListenerInterface,
  MQProducerInterface,
  MQListenerFunc,
  MQMessage,
} from '@sensorbucket/message-queue';
import { EventEmitter } from 'events';

/**
 * AMQP Channel exposes most of the AMQP API such as consuming and producing
 */
export class AMQPChannel implements MQListenerInterface, MQProducerInterface {
  private opened: boolean = false;

  constructor(public readonly channel: amqp.Channel) {}

  private async acknowledge(message: amqp.ConsumeMessage): Promise<void> {
    this.channel.ack(message);
  }

  /**
   * Convert an AMQP message to MQMessage and call the given listener
   *
   * This function can be used in combination with .bind to create a wrapper for listeners
   * @param listener The listener to wrap
   * @param message The received AMQP message
   */
  private onMessage(
    listener: MQListenerFunc,
    message: amqp.ConsumeMessage | null,
  ): void {
    if (message === null) return;
    // Catch message content parsing errors
    let body;
    try {
      body = JSON.parse(message.content.toString());
    } catch (e) {
      throw new Error('Received message contents cannot be parsed to JSON!');
    }
    // Build MQMessage
    const emitMessage: MQMessage = {
      id: message.properties.messageId,
      body,
      acknowledge: this.acknowledge.bind(this, message),
    };
    // Call the listener
    listener(emitMessage);
  }

  async addListener(queue: string, listener: MQListenerFunc): Promise<string> {
    const consumer = await this.channel.consume(
      queue,
      this.onMessage.bind(this, listener),
    );
    return consumer.consumerTag;
  }

  async removeListener(queue: string, listenerTag: string): Promise<void> {
    this.channel.cancel(listenerTag);
  }

  async close(): Promise<void> {
    if (this.channel !== undefined && this.channel !== null)
      return await this.channel.close();
  }

  async produce(queue: string, message: any): Promise<void> {
    this.channel.sendToQueue(queue, Buffer.from(JSON.stringify(message)));
  }
}
