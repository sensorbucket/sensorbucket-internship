import * as amqp from 'amqplib';
import { AMQPChannel } from './channel';

/**
 * Represents a connection to the AMQP server that can create channels
 *
 * An application generally has one of these connections.
 */
export class AMQPConnection {
  private _connection!: amqp.Connection;
  public get connection() {
    return this._connection;
  }

  constructor(private connectionString: string) {}

  /**
   * Connect to the AMQP server
   */
  async connect(): Promise<void> {
    if (this.connection !== undefined && this.connection !== null) return;
    this._connection = await amqp.connect(this.connectionString);
  }

  /**
   * Create a new queue channel for producing and consuming
   * @param queueName The name of the queue
   * @param assertOptions Options for queue behaviour
   * @param consumeOptions Options for consuming behaviour
   */
  async openChannel(): Promise<AMQPChannel> {
    const rawChannel = await this.connection.createChannel();
    const channel = new AMQPChannel(rawChannel);
    return channel;
  }

  /**
   * Close the connection to the AMQP server
   *
   * This will also close all channels and thus all listeners and producers.
   */
  async close(): Promise<void> {
    if (this.connection !== undefined && this.connection !== null)
      await this.connection.close();
  }
}
