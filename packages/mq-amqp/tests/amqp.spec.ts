import { suite, test, slow, retries, skip } from 'mocha-typescript';
import { verify, anything, spy } from 'ts-mockito';
import { AMQPChannel } from '../src/channel';
import { AMQPConnection } from '../src/connection';
import { MQMessage } from '@sensorbucket/message-queue';

class Consumer {
  async onMessage(message: MQMessage): Promise<void> {
    await message.acknowledge();
  }
}

@suite.skip
class AMQPTest {
  private static connection: AMQPConnection;
  private queueName: string = 'test-queue';
  private queue!: AMQPChannel;
  private listenerTag!: string;
  private listenerSpy!: Consumer;

  static async before() {
    /*
    Before all tests, create a global connection which will be used to create channels
    Every test will create a new channel.
    */
    AMQPTest.connection = new AMQPConnection('amqp://localhost:5672');
    await AMQPTest.connection.connect();
  }

  static async after() {
    /*
    After all tests, close global connection
    */
    await AMQPTest.connection.close();
  }

  async before() {
    /*
    Create a channel/queue
    Add a listener and spy it
    */
    this.queue = await AMQPTest.connection.openChannel();
    const listener = new Consumer();
    this.listenerSpy = spy(listener);
    this.listenerTag = await this.queue.addListener(
      this.queueName,
      listener.onMessage,
    );
  }

  async after() {
    if (this.queue !== undefined && this.queue !== null) {
      await this.queue.close();
    }
  }

  /**
   * If we produce a message we should also receive it.
   *
   * (because we have a listener attached)
   */
  @test
  @slow(1000)
  @retries(3)
  async should_send_and_receive() {
    await this.queue.produce(this.queueName, { hello: 'world' });
    // Give the message some time to return
    await new Promise(resolve => setTimeout(resolve, 100));
    // Assert that onMessage was called
    verify(this.listenerSpy.onMessage(anything())).called();
  }

  @test
  @slow(1000)
  @retries(3)
  async should_not_receive_on_removed_listener() {
    this.queue.removeListener(this.queueName, this.listenerTag);
    await this.queue.produce(this.queueName, { hello: 'world' });
    // Give the message some time to return
    await new Promise(resolve => setTimeout(resolve, 100));
    // Assert that onMessage was called
    verify(this.listenerSpy.onMessage(anything())).never();
  }
}
