export class Location {
  constructor(
    public id: number | null,
    public name: string,
    public owner: number,
    public latitude: number,
    public longitude: number,
  ) {}

  /**
   * Create a location from an object
   * @param raw An object representing a Location
   */
  static from(raw: any): Location {
    return new Location(
      raw.id || null,
      raw.name,
      raw.owner,
      raw.latitude,
      raw.longitude,
    );
  }

  /**
   * Get the object as GeoJSON
   */
  get geometry() {
    return {
      type: 'Point',
      coordinates: [this.longitude, this.latitude],
    };
  }

  /**
   * Set coordinates from GeoJSON
   */
  set geometry(value) {
    this.longitude = value.coordinates[0];
    this.latitude = value.coordinates[1];
  }
}
