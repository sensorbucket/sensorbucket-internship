export class SourceType {
  constructor(
    public id: number | null,
    public name: string,
    public configurationSchema: { [index: string]: any },
  ) {}

  public static from(raw: any) {
    return new SourceType(
      raw.id || null,
      raw.name,
      raw.configurationSchema || null,
    );
  }
}
