import { DeviceType } from './deviceType';
import { Source } from './source';

export class Device {
  constructor(
    public id: number | null,
    public name: string,
    public owner: number,
    public deviceType: number | DeviceType,
    public source: number | Source | null,
    public location: number | null,
    public deviceConfiguration?: { [index: string]: any },
    public sourceConfiguration?: { [index: string]: any },
  ) {}

  /**
   * Creates a Device model from an object
   * @param raw An untyped object
   */
  static from(raw: any): Device {
    return new Device(
      raw.id || null,
      raw.name,
      raw.owner,
      raw.deviceType,
      raw.source || null,
      raw.location || null,
      raw.deviceConfiguration || null,
      raw.sourceConfiguration || null,
    );
  }
}
