export class DeviceType {
  constructor(
    public id: number | null,
    public name: string,
    public version: string,
    public configurationSchema: { [index: string]: any },
  ) {}

  public static from(raw: any) {
    return new DeviceType(
      raw.id || null,
      raw.name,
      raw.version,
      raw.configurationSchema || null,
    );
  }
}
