import { SourceType } from './sourceType';

export class Source {
  constructor(
    public id: number | null,
    public name: string,
    public owner: number,
    public sourceType: number | SourceType,
  ) {}

  /**
   * Creates a Source model from an object
   * @param raw An untyped object
   */
  static from(raw: any): Source {
    return new Source(raw.id || null, raw.name, raw.owner, raw.sourceType);
  }
}
