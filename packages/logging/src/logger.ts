import Pino from 'pino';

// Use pretty print in anything besides production
let prettyPrint: any = false;
let logDisabled = false;
if (process.env.NODE_ENV !== 'production' && process.env.NODE_ENV !== 'prod') {
  prettyPrint = {
    colorize: true,
  };
  // If in development and LOG_DISABLED == true or 1 then disable logging
  logDisabled =
    process.env.LOG_DISABLED == '1' || process.env.LOG_DISABLED == 'true';
}

export const logger = Pino({
  prettyPrint,
  enabled: !logDisabled,
});
