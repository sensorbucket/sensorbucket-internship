# Building stage handles lerna monorepo
FROM node:12

WORKDIR /opt/sensorbucket

# Copy monorepo to build-image
COPY . /opt/sensorbucket

# Install dependencies
RUN yarn build:full
